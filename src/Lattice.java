import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Mata on 22-May-17.
 */
public class Lattice {
    Cell[][] cells;
    Cell[][] bufferCells;
    int rows, cols, states;
    List<Change> latticeChanges;

    public Lattice() {
        this(100, 100, 2, false);
    }

    public Lattice(int rows, int cols, int states, boolean testSet) {
        this.rows = rows;
        this.cols = cols;
        this.states = states;

        if (testSet) {
            this.cells = createCells(rows, cols, 1);
            this.bufferCells = createCells(rows, cols, 1);

            create_pentadecathlon(10, 10);
            create_beacon(7, 30);
            create_block(5, 40);
            create_glider(5, 50);
            create_lwss(30, 30);
        } else {
            this.cells = createCells(rows, cols, states);
            this.bufferCells = createCells(rows, cols, states);
        }
        connectCells(this.cells);
        connectCells(this.bufferCells);

        System.out.println(String.format("Lattice | rows: %s cols: %s", cells.length, cells[0].length));

    }

    public static void main(String[] args) {
        System.out.println("19 % 10: " + 19 % 10);
        System.out.println("-9 % 10: " + -9 % 10);
        Cell[] cells = new Cell[10];
        System.out.println(cells[0]);
        Lattice lattice = new Lattice(512, 512, 2, true);
        int updateCount = 0;
        while (updateCount < 100) {
            long startTime = System.nanoTime();
            lattice.update();
            long endTime = System.nanoTime();
            BigDecimal duration = BigDecimal.valueOf(endTime).subtract(BigDecimal.valueOf(startTime)).divide(BigDecimal.valueOf(1_000_000_000L), 10, BigDecimal.ROUND_UP);
            System.out.println(String.format("Update %-5s | Time(s): %s Changes: %5s", updateCount++, duration.toPlainString(), lattice.latticeChanges.size()));
            lattice.display();
        }
    }


    public void update() {
        this.latticeChanges = new LinkedList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                Cell cell = this.cells[row][col];
                Cell bufferCell = this.bufferCells[row][col];
                bufferCell.setState(updateRule(cell));
                if (cell.getState() != bufferCell.getState()) {
                    latticeChanges.add(new Change(row, col, bufferCell.getState()));
                }
            }
        }
        Cell[][] temp = cells;
        cells = bufferCells;
        bufferCells = temp;

    }

    public void display() {
        String display = "Display:\n";
        for (Cell[] row : cells) {
            display += Arrays.stream(row).map(value -> String.valueOf(value.getState())).collect(Collectors.joining());
            display += "\n";
        }
        System.out.println(display);
    }


    private int updateRule(Cell cell) {
        int sum = cell.neighboursSum();
        int state = cell.getState();
        if (state == 0 && sum == 3) return 1;
        if (state == 1 && 2 <= sum && sum <= 3) return 1;
        return 0;
    }

    public Cell[][] createCells(int rows, int cols, int states) {
        System.out.println("Creating cells...");
        Cell[][] cells = new Cell[rows][cols];
        Random random = new Random();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col] = new Cell(row, col, random.nextInt(states));
            }
        }
        return cells;
    }

    public void connectCells(Cell[][] cells) {
        System.out.println("Connecting cells...");
        int rows = cells.length;
        int cols = cells[0].length;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col].setNw(cells[Math.floorMod((row - 1), rows)][
                        Math.floorMod((col - 1), cols)]);
                cells[row][col].setN(cells[Math.floorMod((row - 1), rows)][
                        Math.floorMod((col + 0), cols)]);
                cells[row][col].setNe(cells[Math.floorMod((row - 1), rows)][
                        Math.floorMod((col + 1), cols)]);
                cells[row][col].setE(cells[Math.floorMod((row + 0), rows)][
                        Math.floorMod((col - 1), cols)]);
                cells[row][col].setSe(cells[Math.floorMod((row + 0), rows)][
                        Math.floorMod((col + 1), cols)]);
                cells[row][col].setS(cells[Math.floorMod((row + 1), rows)][
                        Math.floorMod((col - 1), cols)]);
                cells[row][col].setSw(cells[Math.floorMod((row + 1), rows)][
                        Math.floorMod((col + 0), cols)]);
                cells[row][col].setW(cells[Math.floorMod((row + 1), rows)][
                        Math.floorMod((col + 1), cols)]);
            }
        }
    }

    public int getBoundedRow(int row, int row_offset) {
        return Math.floorMod((row + row_offset), rows);
    }

    public int getBoundedCol(int col, int col_offset) {
        return Math.floorMod((col + col_offset), cols);
    }

    public void create_glider(int row, int col) {
        cells[getBoundedRow(row, -1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 1)].setState(1);
    }

    public void create_lwss(int row, int col) {
        cells[getBoundedRow(row, -1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, -1)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, -2)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, 2)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, -2)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 2)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, 2)][getBoundedCol(col, 0)].setState(1);
    }

    public void create_beacon(int row, int col) {
        cells[getBoundedRow(row, -1)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, -1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 2)].setState(1);
        cells[getBoundedRow(row, 2)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 2)][getBoundedCol(col, 2)].setState(1);
    }

    public void create_pentadecathlon(int row, int col) {
        cells[getBoundedRow(row, -4)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, -3)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, -2)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, -2)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, -1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 2)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 3)][getBoundedCol(col, -1)].setState(1);
        cells[getBoundedRow(row, 3)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 4)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 5)][getBoundedCol(col, 0)].setState(1);
    }

    public void create_block(int row, int col) {
        cells[getBoundedRow(row, 0)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 0)][getBoundedCol(col, 1)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 0)].setState(1);
        cells[getBoundedRow(row, 1)][getBoundedCol(col, 1)].setState(1);
    }
}

class Cell {

    int state, row, col;
    Cell nw, n, ne, e, se, s, sw, w;
    Cell[] neighbours;

    public Cell(int row, int col, int state) {
        this.state = state;
        this.row = row;
        this.col = col;
        this.neighbours = new Cell[8];
    }

    public int neighboursSum() {
        int sum = 0;
        for (Cell n : neighbours) {
            sum += n.getState();
        }
        return sum;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public Cell getNw() {
        return nw;
    }

    public void setNw(Cell nw) {
        this.nw = nw;
        neighbours[0] = nw;
    }

    public Cell getN() {
        return n;
    }

    public void setN(Cell n) {
        this.n = n;
        neighbours[1] = n;
    }

    public Cell getNe() {
        return ne;
    }

    public void setNe(Cell ne) {
        this.ne = ne;
        neighbours[2] = ne;
    }

    public Cell getE() {
        return e;
    }

    public void setE(Cell e) {
        this.e = e;
        neighbours[3] = e;
    }

    public Cell getSe() {
        return se;
    }

    public void setSe(Cell se) {
        this.se = se;
        neighbours[4] = se;
    }

    public Cell getS() {
        return s;
    }

    public void setS(Cell s) {
        this.s = s;
        neighbours[5] = s;
    }

    public Cell getSw() {
        return sw;
    }

    public void setSw(Cell sw) {
        this.sw = sw;
        neighbours[6] = sw;
    }

    public Cell getW() {
        return w;
    }

    public void setW(Cell w) {
        this.w = w;
        neighbours[7] = w;
    }

}

class Change {
    int row, col, state;

    public Change(int row, int col, int state) {
        this.row = row;
        this.col = col;
        this.state = state;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
