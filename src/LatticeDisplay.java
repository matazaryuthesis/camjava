import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.math.BigDecimal;

/**
 * Created by Mata on 23-May-17.
 */
public class LatticeDisplay extends Application {
    public static void main(String[] args) {
        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Lattice Display");
        StackPane root = new StackPane();
        int canvasWidth = 512, canvasHeight = 512;
        int sceneWidth = canvasWidth, sceneHeight = canvasHeight + 40;

        Text text = new Text("sadasdasd");
        text.setFont(new Font(15));
        text.setFill(Color.RED);


        Canvas canvas = new Canvas(canvasWidth, canvasHeight);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        root.getChildren().addAll(canvas, text);
        root.setAlignment(text, Pos.BOTTOM_LEFT);

        gc.setFill(Color.BLUE);
        gc.fillRect(75, 75, 100, 100);

        int rows = 512, cols = 512, states = 2;
        boolean testSet = false; // <-------- testSet to false for random lattice
        Lattice lattice = new Lattice(rows, cols, states, testSet);
        Scene s = new Scene(root, sceneWidth, sceneHeight);
        primaryStage.setScene(s);
        primaryStage.show();
        AnimationTimer loop = new AnimationTimer() {
            int updateCount = 0;

            @Override
            public void handle(long now) {
                long startTimeDisplay = System.nanoTime();
                display(lattice, gc, canvasWidth, canvasHeight);
                long startTimeUpdate = System.nanoTime();
                lattice.update();
                long endTimeUpdate = System.nanoTime();
                BigDecimal durationDisplay = BigDecimal.valueOf(startTimeUpdate).subtract(BigDecimal.valueOf(startTimeDisplay)).divide(BigDecimal.valueOf(1_000_000_000L), 10, BigDecimal.ROUND_UP);
                BigDecimal durationUpdate = BigDecimal.valueOf(endTimeUpdate).subtract(BigDecimal.valueOf(startTimeUpdate)).divide(BigDecimal.valueOf(1_000_000_000L), 10, BigDecimal.ROUND_UP);
                String information = String.format("Update#%-5s: %ss Changes: %-6s Display: %-5ss", updateCount++, durationUpdate.toPlainString(), lattice.latticeChanges.size(), durationDisplay.toPlainString());
                text.setText(information);
                System.out.println(information);
            }
        };
        loop.start();
    }

    public void display(Lattice lattice, GraphicsContext gc, int width, int height) {
        int rectHeight = height / lattice.rows;
        int rectWidth = width / lattice.cols;
        for (int row = 0; row < lattice.rows; row++) {
            for (int col = 0; col < lattice.cols; col++) {
                Cell cell = lattice.cells[row][col];
                gc.setFill(cell.getState() == 0 ? Color.WHITE : Color.BLACK);
                gc.fillRect(row * rectHeight, col * rectWidth, rectHeight, rectWidth);
            }
        }

    }

}
